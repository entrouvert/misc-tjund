#!/bin/bash

source ~/envs/publik-env-py3/bin/activate
hobo-manage tenant_command runscript ~/src/misc-tjund/hobo-change-theme.py -d hobo.dev.publik.love --theme $1
deactivate
