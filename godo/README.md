GODO
a ProseMirror editor for Publik
=======

Godo is based on proseMirror
ProseMirror is distributed via npm.
You need nodeJS & npm

First install node modules since this folder

`npm install`



NPM scripts 
-----

`npm run build`

Get all JS modules and create a big JS file in dist/js

> npm run watch

automatique build when JS is modified


Use Godo
-----

import in your webpage 

* dist/css/godo.css
* dist/js.godo.js

Transform a textarea in godo 

  <script type="module">
    import Godo from "./js/godo.js";

    const myGodo = new Godo(textarea, {
      schema: "basic"
    });
  </script>

For more examples, see dist/index.html.


