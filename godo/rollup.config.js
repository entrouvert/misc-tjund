import buble from '@rollup/plugin-buble';
import { nodeResolve } from '@rollup/plugin-node-resolve';

export default {
  input: './src-js/godo.js',
  output: [
    {
      file: 'dist/js/godo.js',
      format: 'es',
      sourcemap: true
    }
  ],
  plugins: [nodeResolve(), buble()]
}
