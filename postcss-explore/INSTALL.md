# Installing PostCSS postcss-explore

[PostCSS postcss-explore] runs in all Node environments, with special instructions for:

| [Node](#node) | [PostCSS CLI](#postcss-cli) | [Webpack](#webpack) | [Create React App](#create-react-app) | [Gulp](#gulp) | [Grunt](#grunt) |
| --- | --- | --- | --- | --- | --- |

## Node

Add [PostCSS postcss-explore] to your project:

```bash
npm install postcss-postcss-explore --save-dev
```

Use **PostCSS postcss-explore** to process your CSS:

```js
const postcssPostcssExplore = require('postcss-postcss-explore');

postcssPostcssExplore.process(YOUR_CSS /*, processOptions, pluginOptions */);
```

Or use it as a [PostCSS] plugin:

```js
const postcss = require('postcss');
const postcssPostcssExplore = require('postcss-postcss-explore');

postcss([
  postcssPostcssExplore(/* pluginOptions */)
]).process(YOUR_CSS /*, processOptions */);
```

## PostCSS CLI

Add [PostCSS CLI] to your project:

```bash
npm install postcss-cli --save-dev
```

Use **PostCSS postcss-explore** in your `postcss.config.js` configuration file:

```js
const postcssPostcssExplore = require('postcss-postcss-explore');

module.exports = {
  plugins: [
    postcssPostcssExplore(/* pluginOptions */)
  ]
}
```

## Webpack

Add [PostCSS Loader] to your project:

```bash
npm install postcss-loader --save-dev
```

Use **PostCSS postcss-explore** in your Webpack configuration:

```js
const postcssPostcssExplore = require('postcss-postcss-explore');

module.exports = {
  module: {
    rules: [
      {
        test: /\.css$/,
        use: [
          'style-loader',
          { loader: 'css-loader', options: { importLoaders: 1 } },
          { loader: 'postcss-loader', options: {
            ident: 'postcss',
            plugins: () => [
              postcssPostcssExplore(/* pluginOptions */)
            ]
          } }
        ]
      }
    ]
  }
}
```

## Create React App

Add [React App Rewired] and [React App Rewire PostCSS] to your project:

```bash
npm install react-app-rewired react-app-rewire-postcss --save-dev
```

Use **React App Rewire PostCSS** and **PostCSS postcss-explore** in your
`config-overrides.js` file:

```js
const reactAppRewirePostcss = require('react-app-rewire-postcss');
const postcssPostcssExplore = require('postcss-postcss-explore');

module.exports = config => reactAppRewirePostcss(config, {
  plugins: () => [
    postcssPostcssExplore(/* pluginOptions */)
  ]
});
```

## Gulp

Add [Gulp PostCSS] to your project:

```bash
npm install gulp-postcss --save-dev
```

Use **PostCSS postcss-explore** in your Gulpfile:

```js
const postcss = require('gulp-postcss');
const postcssPostcssExplore = require('postcss-postcss-explore');

gulp.task('css', () => gulp.src('./src/*.css').pipe(
  postcss([
    postcssPostcssExplore(/* pluginOptions */)
  ])
).pipe(
  gulp.dest('.')
));
```

## Grunt

Add [Grunt PostCSS] to your project:

```bash
npm install grunt-postcss --save-dev
```

Use **PostCSS postcss-explore** in your Gruntfile:

```js
const postcssPostcssExplore = require('postcss-postcss-explore');

grunt.loadNpmTasks('grunt-postcss');

grunt.initConfig({
  postcss: {
    options: {
      use: [
       postcssPostcssExplore(/* pluginOptions */)
      ]
    },
    dist: {
      src: '*.css'
    }
  }
});
```

[Gulp PostCSS]: https://github.com/postcss/gulp-postcss
[Grunt PostCSS]: https://github.com/nDmitry/grunt-postcss
[PostCSS]: https://github.com/postcss/postcss
[PostCSS CLI]: https://github.com/postcss/postcss-cli
[PostCSS Loader]: https://github.com/postcss/postcss-loader
[PostCSS postcss-explore]: https://github.com/sacripant/postcss-postcss-explore
[React App Rewire PostCSS]: https://github.com/csstools/react-app-rewire-postcss
[React App Rewired]: https://github.com/timarney/react-app-rewired
