import postcss from 'postcss';

export default postcss.plugin('postcss-postcss-explore', opts => {

    return (root) => {
        // console.log({ root, result }); // eslint-disable-line no-console
        let startLine = 0;
        root.walkComments( (comment) => {
            if (comment.text == "End of core styles")
                startLine = comment.source.start.line;
        });

        root.walkRules( opts.selector, (rule, index) => {
            const ruleLine = rule.source.start.line;
            if (ruleLine <= startLine) return;

            rule.walkDecls( opts.propertie, decl => {
                console.group(rule.source.input.from);
                console.group(rule.selector);
                console.log(decl.prop, decl.value);
                console.log(decl.source.start);
                console.groupEnd();
                console.groupEnd();
                console.log("");
            });
        });
    };
});