# PostCSS postcss-explore [<img src="https://postcss.github.io/postcss/logo.svg" alt="PostCSS" width="90" height="90" align="right">][postcss]

[![NPM Version][npm-img]][npm-url]
[![Build Status][cli-img]][cli-url]
[![Support Chat][git-img]][git-url]

[PostCSS postcss-explore] lets you ... in CSS.

```pcss
.example { ... }

/* becomes */

.example { ... }
```

## Usage

Add [PostCSS postcss-explore] to your project:

```bash
npm install postcss-postcss-explore --save-dev
```

Use **PostCSS postcss-explore** to process your CSS:

```js
const postcssPostcssExplore = require('postcss-postcss-explore');

postcssPostcssExplore.process(YOUR_CSS /*, processOptions, pluginOptions */);
```

Or use it as a [PostCSS] plugin:

```js
const postcss = require('postcss');
const postcssPostcssExplore = require('postcss-postcss-explore');

postcss([
  postcssPostcssExplore(/* pluginOptions */)
]).process(YOUR_CSS /*, processOptions */);
```

**PostCSS postcss-explore** runs in all Node environments, with special instructions for:

| [Node](INSTALL.md#node) | [PostCSS CLI](INSTALL.md#postcss-cli) | [Webpack](INSTALL.md#webpack) | [Create React App](INSTALL.md#create-react-app) | [Gulp](INSTALL.md#gulp) | [Grunt](INSTALL.md#grunt) |
| --- | --- | --- | --- | --- | --- |

## Options

...

[cli-img]: https://img.shields.io/travis/sacripant/postcss-postcss-explore/master.svg
[cli-url]: https://travis-ci.org/sacripant/postcss-postcss-explore
[git-img]: https://img.shields.io/badge/support-chat-blue.svg
[git-url]: https://gitter.im/postcss/postcss
[npm-img]: https://img.shields.io/npm/v/postcss-postcss-explore.svg
[npm-url]: https://www.npmjs.com/package/postcss-postcss-explore

[PostCSS]: https://github.com/postcss/postcss
[PostCSS postcss-explore]: https://github.com/sacripant/postcss-postcss-explore
