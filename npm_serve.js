const pkgJson = require('./package.json');
// const exec = require('child_process').exec;
const browserSync = require("browser-sync").create();
const apps = [ 'combo', 'wcs', 'authentic'];

const param = process.argv.pop();
const themeFolder = process.env.npm_package_config_themefolder;
const theme = process.env.npm_package_config_theme;
const app =  apps.includes(param) ? param : process.env.npm_package_config_app;
const appurl = pkgJson.config.url[app];

browserSync.init({
    proxy: appurl,
    files: [
      `${themeFolder}/static/${theme}/*.css`,
      `${themeFolder}/templates/{theme}/*`
    ],
    logLevel: "debug"
})
