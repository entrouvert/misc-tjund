# -*- coding: utf-8 -*-
#
# Change Publik theme
#
# Usage:
#  --theme THEME         -- theme to shot (substring match)
#
# Example:
#  hobo-manage tenant_command runscript hobo-change-theme.py -d hobo.dev.publik.love --theme new-theme-id
#

from hobo.theme.utils import get_selected_theme, set_theme
from hobo.deploy.signals import notify_agents
import sys
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('--theme', dest='theme', type=str)
args = parser.parse_args()

theme_id = args.theme
if get_selected_theme() != theme_id:
	set_theme(theme_id)
	notify_agents(None)
print(get_selected_theme())
