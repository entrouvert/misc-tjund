#!/bin/bash

# folder Name
read -p 'folder name : ' folderName
# Theme Label
read -p 'Theme label : ' themeLabel
# Theme Color
read -p 'Theme Color (hexa) : ' themeColor
# custom template
read -p 'Need custom templates (0/1) : ' variant

cd ~/src/publik-base-theme/

# create folders
staticfolderPath=static/$folderName
mkdir $staticfolderPath
if [ $variant == 1 ]
then
  variantfolderPath=templates/variants/$folderName
  mkdir $variantfolderPath
fi

# create config.json file
cat >${staticfolderPath}/config.json <<EOF
{
  "label": "${themeLabel}",
  "variables": {
    "theme_color": "${themeColor}",
    "email_header_asset": "emails:logo"
  },
  "settings": {
    "combo": {
      "COMBO_ASSET_SLOTS.update": {
        "header:logo": { "label": "Têtière : logo" },
        "emails:logo": {"label": "Emails : logo"}
      }
    }
  }
}
EOF

# create css files
touch ${staticfolderPath}/_custom.scss

cat >${staticfolderPath}/_vars.scss <<EOF
\$base-font: 16;
// Core vars
\$width: 1280em / \$base-font;
\$edge-gutter: 1em;
\$font-size: 100% / ( 16 / \$base-font );
\$mobile-limit: 64em;
\$very-small-limit: 35em;
\$primary-color: ${themeColor};
\$toplinks-style: none;
\$widget-unique-checkbox-position: left;
\$form-title-style: none;
EOF

cat >${staticfolderPath}/style.scss <<EOF
@charset "UTF-8";
@import '../includes/fonts/source-sans-pro';

@import 'vars';
@import '../includes/publik';
@import 'custom';
EOF

# make
python3 setup.py build_themes_json
python3 setup.py compile_scss
